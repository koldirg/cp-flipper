set :site_title, "CustomPublish Flipper"
set :site_url, "http://www.custompublish.com"
set :site_description, "Meta description."
set :site_keywords, "keyword-one, keyword-two"
set :site_author, "mindResin"
set :site_analyticsID, "UA-XXXXX-X"

#Initiate LiveReload
#activate :livereload, :host => '0.0.0.0'

###
# Slim
###
require 'slim'
set :slim, :layout_engine => :slim
Slim::Engine.set_default_options :pretty => true, :sort_attrs => false
Slim::Engine.disable_option_validator!

###
# Compass
###
compass_config do |config|
  config.output_style = :expanded
  config.sass_options = { :line_comments => false, :debug_info => false }
end

###
# Helpers
###
helpers do
  def nav_active(page)
    @page_id == page ? {:class => "menu_selected"} : {}
  end
end

# Automatic image dimensions on image_tag helper
# activate :automatic_image_sizes

###
# Directory Indexes for Pretty URL's
###

# activate :directory_indexes
# set :index_file, "index.php"
# page "/i-really-want-the-extension.html", :directory_index => false

set :css_dir, 'assets/css'
set :js_dir, 'assets/js'
set :images_dir, 'assets/images'
set :relative_links, false

# Build-specific configuration
configure :build do
  compass_config do |config|
    config.output_style = :compact
    config.sass_options = { :line_comments => false, :debug_info => false }
  end
  # activate :minify_css
  # activate :minify_javascript
  # activate :cache_buster
  activate :relative_assets
  # activate :smusher
  # set :http_path, "/Content/images/"
end

activate :deploy do |deploy|
  deploy.method = :ftp
  # host, user, password and path *must* be set
  deploy.host = "mindresin.co.uk"
  deploy.user = "flip2@mindresin.co.uk"
  deploy.password = "flipflip2"
  deploy.path = "/"
end