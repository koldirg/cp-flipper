# General purpose delay method
window.delay = (ms, func) -> setTimeout func, ms


#=======================================
#	PAGE Management
#=======================================
window.addPage = (page, book) ->
	window.pages = book.turn("pages")

	element = $('<div />', {})															# Creates the new element for this page
	if book.turn("addPage", element, page)
		element.html '<div class="gradient"></div><div class="loader"></div>'			# Add loader and gradient
		loadPage(page, element)															# Load page

window.loadPage = (page, pageElement) ->
	img = $('<img />')
	
	img.mousedown (event) ->
		event.preventDefault();

	img.load ->																			# Set Size
		$(this).css
			width: "100%"
			height: "100%"
		$(this).appendTo(pageElement)													# Add image to page after it's loaded
		pageElement.find('.loader').remove()											# Remove loader

	img.attr('src', 'pages/' +  page + '.jpg')											# Loads pageElement

	loadRegions(page, pageElement)

#=======================================
#	ZOOM
#=======================================
window.zoomTo = (event) ->
	delay 1, ->
		if viewport.data().regionclicked
			viewport.data().regionclicked = false
		else
			if viewport.zoom("value") == 1
				viewport.zoom("zoomIn", event)
			else
				viewport.zoom("zoomOut")

## Deprecated REGIONS

#=======================================
#	USER AGENT Chrome
#=======================================
window.isChrome = ->
	return navigator.userAgent.indexOf('Chrome')!=-1;

#=======================================
#	NAVIGATION BUTTON Disabler
#=======================================
window.disableControls = (page) ->
	
	if page == 1 or page == 2 or page == 3												# Conditionally Disable PREVIOUS Buttons
		$('.previous-button').hide()
		$('.previous-icon a, .firstpage-icon a').css
			'opacity': 0.2
			'pointer-events': 'none'
			'cursor': 'normal'
		.parent('li').css
			'pointer-events': 'none'
			'cursor': 'default'
	else
		$('.previous-button').show()
		$('.previous-icon a, .firstpage-icon a').css
			'opacity': 1
			'pointer-events': 'all'
			'cursor': 'normal'

	if page == flipbook.turn('pages')													# Conditionally Disable NEXT Buttons
		$('.next-button').hide()
		$('.next-icon a').css
			'opacity': 0.5
			'pointer-events': 'none'
		.parent('li').css
			'pointer-events': 'none'
			'cursor': 'default'
	else
		$('.next-button').show()
		$('.next-icon a').css
			'opacity': 1
			'pointer-events': 'all'

#=======================================
#	VIEWPORT Set Width & Height
#=======================================
window.resizeViewport = ->
	width = $(window).width()
	height = $(window).height()
	options = flipbook.turn('options')

	flipbook.removeClass('animated')
	viewport.css
		width: width
		height: height
	.zoom('resize')

	# Size
	if flipbook.turn('zoom') == 1
		bound = calculateBound
			width: options.width
			height: options.height
			boundWidth: Math.min(options.width, width)
			boundHeight: Math.min(options.height, height)

		if bound.width % 2 isnt 0
			bound.width-=1

		if bound.width isnt flipbook.width() or bound.height isnt flipbook.height()		# 
			flipbook.turn('size', bound.width, bound.height)							# investigate this method further for the layout switch TODO

			if flipbook.turn('page') is 1
				flipbook.turn('peel', 'br')

		flipbook.css
			top: -bound.height / 2
			left: -bound.width / 2

	# Offset
	magazineOffset = flipbook.offset()
	boundH = height - magazineOffset.top - flipbook.height()
	marginTop = boundH - $('.thumbnails > div').height() /2

	if marginTop < 0
		$('.thumbnails').css height: 1
	else
		$('.thumbnails').css
			height: boundH
		$('.thumbnails > div').css
			marginTop: marginTop

#=======================================
#	VIEWS
#=======================================

# Count Views
window.numberOfViews = (book) ->
	return book.turn('pages') / 2 + 1

# Current View
window.getViewNumber = (book, page) ->
	return parseInt (page or book.turn('page')) / 2 + 1, 10

window.largeMagazineWidth = ->
	return 2214																				# Width of book when ZOOMED in

#=======================================
#	Decode URL Parameters
#=======================================
window.decodeParams = (data) ->
	parts = data.split('&')
	obj = {}

	for num in parts
		d = parts[i].split('=')
		obj[decodeURIComponent(d[0])] = decodeURIComponent(d[1])
	return obj;

#=======================================
#	 Calculate the width and height of a square within another square
#=======================================
window.calculateBound = (d) ->
	bound =
		width: d.width
		height: d.height

	if bound.width > d.boundWidth or bound.height > d.boundHeight
		rel = bound.width / bound.height

		if d.boundWidth / rel > d.boundHeight and d.boundHeight * rel <= d.boundWidth
			bound.width = Math.round(d.boundHeight * rel)
			bound.height = d.boundHeight
		else
			bound.width = d.boundWidth
			bound.height = Math.round(d.boundWidth / rel)
	return bound