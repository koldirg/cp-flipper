window.flipbook = $('.magazine')
window.viewport = $('.magazine-viewport')

loadApp = ->

	# Strip the elements that CustomPublish injects into BODY view
	$(".innerbody img").unwrap()
	$("center").remove()

	if flipbook.width == 0 or flipbook.height == 0
		delay 10, loadApp
		return

#=======================================
#	FLIP BOOK Initialize
#=======================================
	flipbook.turn
		duration: 1000
		acceleration: !isChrome()
		gradients: true
		autoCenter: true
		elevation: 10
		display: "double"
		page: 2
		when:
			turning: (event, page, view) ->
				book = $(this)
				currentPage = book.turn('page')
				pages = book.turn('pages')
				Hash.go('page/' + page).update()
				disableControls(page)

				book.addClass('turning')
				#$('.thumbnails .page-'+currentPage).parent().removeClass('current');
				#$('.thumbnails .page-'+page).parent().addClass('current');

			turned: (event, page, view) ->
				disableControls(page)
				$(this).turn('center').removeClass('turning')
				if page == 1
					$(this).turn('peel', 'br')

			missing: (event, pages) ->
				for num in pages
					addPage(pages[num], $(this));

#=======================================
#	ZOOM Initialize
#=======================================
	viewport.zoom
		flipbook: flipbook
		max: ->
			largeMagazineWidth()/flipbook.width();
		when:
			swipeLeft: ->
				$(this).zoom('flipbook').turn('next')
			swipeRight: ->
				$(this).zoom('flipbook').turn('previous')
			resize: (event, scale, page, pageElement) ->
				
			zoomIn: ->
				# $('.thumbnails').hide()
				flipbook.removeClass('animated').addClass('zoom-in')
				# Changes the Zoom icon in the toolbar
				$('.zoom-icon').removeClass('zoom-icon-in').addClass('zoom-icon-out')
				# isTouch refers to mobile device
				if not window.escTip and not $.isTouch
					escTip = true

					$('<div />', {'class': 'exit-message'}).
						html('<div>Trykk ESC for å avslutte</div>').
							appendTo($('body')).
							delay(2000).
							animate
								opacity: 0
								500
								-> $(this).remove()

			zoomOut: ->
				# $('.thumbnails').fadeIn();
				$('.exit-message').hide();
				# Changes the Zoom icon in the toolbar
				$('.zoom-icon').removeClass('zoom-icon-out').addClass('zoom-icon-in');
				delay 0, ->
					flipbook.addClass('animated').removeClass('zoom-in')
					resizeViewport

#=======================================
#	ZOOM Events
#=======================================
	noZoom = [6, 7, 17, 18, 20]

	if $.isTouch
		viewport.bind "zoom.doubleTap", zoomTo
	else
		viewport.bind "zoom.doubleTap", zoomTo

#=======================================
#	DISABLE First View
#=======================================

	flipbook.bind "turning", (event, page) ->
		if page == 1
			event.preventDefault()

	flipbook.bind "start", (event, pageObject) ->
		if pageObject.page == 1
			event.preventDefault()

#=======================================
#	ARROW KEY Navigation
#=======================================

	$(document).keydown (e) ->
		previous = 37
		next = 39
		esc = 27
		zoomIn = 38
		zoomOut = 40

		switch e.keyCode
			when previous
				flipbook.turn('previous')
				e.preventDefault()
			when next
				flipbook.turn('next');
				e.preventDefault();
			when zoomIn
				viewport.zoom('zoomIn');	
				e.preventDefault();
			when esc, zoomOut
				viewport.zoom('zoomOut');	
				e.preventDefault();

#=======================================
#	HASH URI Navigation
#=======================================
	Hash.on '^page\/([0-9]*)$',
		yep: (path, parts) ->
			page = parts[1]

			if page isnt undefined
				if flipbook.turn('is')
						flipbook.turn('page', page)
		nop: (path) ->
			if flipbook.turn('is')
				flipbook.turn('page', 1);						#	Consider changing this to match the 'double view on start' statements above

#=======================================
#	Window RESIZE
#=======================================
	$(window).resize (event) ->
		resizeViewport()
	.bind "orientationchange", ->
		resizeViewport()

#=======================================
#	NAVIGATION Buttons
#=======================================
	$('.next-button, .next-icon')
	#.bind $.mouseEvents.over, ->
	#	flipbook.turn("peel", "br")
	#.bind $.mouseEvents.out, ->
	#	flipbook.turn("peel", false)
	.click ->
		flipbook.turn("next")

	$('.previous-button, .previous-icon')
	#.bind $.mouseEvents.over, ->
	#	flipbook.turn("peel", "bl")
	#.bind $.mouseEvents.out, ->
	#	flipbook.turn("peel", false)
	.click ->
		flipbook.turn("previous")

	resizeViewport();

	flipbook.addClass('animated')

#=======================================
#	PRINT Button
#=======================================
	$('.print-icon').click (event) ->

		printView = flipbook.turn('view')
		printWindow = window.open '', '', 'resizable=1, left=0, top=0, width=800, height=600, toolbar=0'

		$.each printView, (index, value) ->
			if value is null or value is 0 or value is 1 or value is 2
				event.preventDefault()
			else
				queuePage = $("div").find("[page='" + value + "']").find "img.page"
				printUrl = (queuePage).attr("src")
				printWindow.document.write "<img src='" + printUrl + "'/><br />"
		printWindow.focus()
		printTimer = delay 5000, -> printWindow.window.print()

#=======================================
#	ZOOM Button
#=======================================
	$('.zoom-icon').click ->
		if $(this).hasClass('zoom-icon-in')
			viewport.zoom('zoomIn');
		else if $(this).hasClass('zoom-icon-out')
			viewport.zoom('zoomOut');

#	Mousewheel functions, need work TODO
#
#	viewport.bind "mousewheel", (e, delta) ->
#		if delta > 0
#			console.log("wut")
#		else
#			console.log("nopesd")


#=======================================
#	FIRSTPAGE Button
#=======================================
	$('.firstpage-icon').click (event) ->
		flipbook.turn('page', 2);

#=======================================
#	AUTHENTICATE Button
#=======================================
	$('.authenticate-icon').click (event) ->
		flipbook.turn('page', 40);

loadApp();