# jQuery
// = require '_vendor/_jquery-1.9.1.min'
# modernizer (Depricate?)
// = require '_vendor/_modernizr.2.5.3.min'
# Gesture Library
// = require '_jgestures.min'
# Hash URIs
// = require '_hash'
# The Turn.js Framework
// = require '_turn.min'
# Zoom.js
// = require '_zoom.min'
# Magazine Functions
// = require 'cpflip_methods'
# Lightbox
// = require '_slimbox2'