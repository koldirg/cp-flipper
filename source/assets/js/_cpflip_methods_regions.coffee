
#=======================================
#	REGIONS																				# Deprecate this section, not needed
#=======================================
loadRegions = (page, element) ->
	$.getJSON('pages/'+page+'-regions.json').											
		done (data) ->
			$.each data, (key, region) ->
				addRegion(region, element)

addRegion = (region, pageElement) ->
	reg = $('<div />', 'class': 'region  ' + region['class'])
	options = flipbook.turn('options')
	pageWidth = options.width / 2
	pageHeight = options.height

	reg.css
		top: Math.round(region.y / pageHeight * 100) + '%'
		left: Math.round(region.x / pageWidth * 100) + '%'
		width: Math.round(region.width / pageWidth * 100) + '%'
		height: Math.round(region.height / pageHeight * 100) + '%'
	.attr('region-data', $.param(region.data||''))

regionClick = (event) ->
	region = $(event.target)

	if region.hasClass('region')
		viewport.data().regionClicked = true
		delay 100, ->
			viewport.data().regionClicked = false

		regionType = $.trim(region.attr('class').replace('region', ''))
		return processRegion(region, regionType);

processRegion = (region, regionType) ->
	data = decodeParams(region.attr('region-data'))

	switch regionType
		when 'link'
			window.open(data.url);
		when 'zoom'
			regionOffset = region.offset()
			viewportOffset = viewport.offset()
			pos =
				x: regionOffset.left - viewportOffset.left
				y: regionOffset.top - viewportOffset.top

			viewport.zoom('zoomIn', pos)
		when 'to-page'
			flipbook.turn('page', data.page)
